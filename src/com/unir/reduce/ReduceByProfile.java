package com.unir.reduce;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;
import java.util.Iterator;

/**
 * Created by Juan Pablo Alvis Colmenares on 15/04/17. mailto:juanpabloalvis@gmail.com
 */
public class ReduceByProfile extends Reducer<Text, IntWritable, Text, DoubleWritable> {
    @Override
    protected void reduce(Text key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
        Double suma = 0.0;
        int cuenta = 1;
        Iterator<IntWritable> iter = values.iterator();
        while (iter.hasNext()){
            IntWritable elem = iter.next();
            suma+=elem.get();
            cuenta=cuenta+1;

        }
        context.write(key, new DoubleWritable(suma/cuenta));
    }
}
