package com.unir.reduce;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by Juan Pablo Alvis Colmenares on 15/04/17. mailto:juanpabloalvis@gmail.com
 */
public class ReduceByUserId extends Reducer<Text, Text, Text, Text>{

    private ArrayList<Text> listA = new ArrayList<Text>();
    private ArrayList<Text> listB = new ArrayList<Text>();

    @Override
    protected void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {

        String tempKey = "";
        int totalClicks = 0;
        //Cleaning the list
        listA.clear();
        listB.clear();
        Iterator<Text> iter = values.iterator();

        while (iter.hasNext()){
            Text next = iter.next();
            String data = next.toString();
            String[] dataOfValue = data.split("\\|");
            if (dataOfValue[0].equals("A")){
                listA.add(new Text(dataOfValue[1]));
            } else if (dataOfValue[0].equals("B")){
                listB.add(new Text(dataOfValue[1]));
            }
        }
        if (!listA.isEmpty() && !listB.isEmpty()) {
            totalClicks = 0;
            for(Text a : listA)  {
                tempKey = a.toString();
                for(Text b : listB)  {
                    totalClicks += Integer.parseInt(b.toString());
                }
            }
            context.write(new Text(tempKey), new Text(totalClicks+""));
        }
    }
}
