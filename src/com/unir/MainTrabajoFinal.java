package com.unir;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hdfs.DistributedFileSystem;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.MultipleInputs;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Iterator;

public class MainTrabajoFinal {


	// Map Job1
    public static class SimpleMap extends Mapper<Text, Text, Text, IntWritable> {
        @Override
        protected void map(Text key, Text value, Context context) throws IOException, InterruptedException {
            String linea = value.toString();
            String[] data = linea.split("\t");

            try {
                final String getCdt = data[11];
                if(getCdt.equals("yes")){
					context.write(new Text("total"), new IntWritable(1));
				}
            } catch (Exception e) {
                System.err.println("Ocurrio un error 1" + e);
            }

        }
    }

    // Reducer Job1
	public static class CountAcceptanceCDT extends Reducer<Text, IntWritable, Text, IntWritable> {

		@Override
		protected void reduce(Text key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
			int cuenta = 0;
			Iterator<IntWritable> iter = values.iterator();
			while (iter.hasNext()) {
				iter.next();
				cuenta = cuenta + 1;
			}
			context.write(key, new IntWritable(cuenta));
		}
	}

	// Map1Job2
	public static class CountMaritalMap extends Mapper<Text, Text, Text, Text> {

    	@Override
        protected void map(Text key, IntWritable value, Context context) throws IOException, InterruptedException {
            int total = value;
            //String[] data = linea.split("\t");

            try {
                context.write(new Text(key), new Text("A" + "|" + value)); // A Marital
            } catch (Exception e) {
                System.err.println("Ocurrio un error al leer el archivo A" + e);
            }

        }
    }

    // Map2Job2
	public static class CountMaritalMap extends Mapper<Text, Text, Text, Text> {
        @Override
        protected void map(Text key, Text value, Context context) throws IOException, InterruptedException {
            String linea = value.toString();
            String[] data = linea.split("\t");

            try {
                context.write(new Text(""), new Text("A" + "|" + data[1])); // A Marital
            } catch (Exception e) {
                System.err.println("Ocurrio un error A" + e);
            }

        }
    }

    public static class TrainingMap extends Mapper<LongWritable, Text, Text, Text> {
        @Override
        protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
            String linea = value.toString();
            String[] data = linea.split("\t");

            try {
                // Se calcula el CTR para cada usuario
                context.write(new Text(data[11]), new Text("B" + "|" + Integer.parseInt(data[0]) / Integer.parseInt(data[1]))); //FileTraining AS B
            } catch (Exception e) {
                System.err.println("Ocurrio un error B" + e);
            }

        }
    }

	public static class CountAcceptance extends Reducer<Text, Text, Text, Text> {

		private ArrayList<Text> listA = new ArrayList<Text>();
		private ArrayList<Text> listB = new ArrayList<Text>();

		@Override
		protected void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {

			String tempKey = "";
			int totalClicks = 0;
			//Cleaning the list
			listA.clear();
			listB.clear();
			Iterator<Text> iter = values.iterator();

			while (iter.hasNext()) {
				Text next = iter.next();
				String data = next.toString();
				String[] dataOfValue = data.split("\\|");
				if (dataOfValue[0].equals("A")) {
					listA.add(new Text(dataOfValue[1]));
				} else if (dataOfValue[0].equals("B")) {
					listB.add(new Text(dataOfValue[1]));
				}
			}
			if (!listA.isEmpty() && !listB.isEmpty()) {
				totalClicks = 0;
				for (Text a : listA) {
					tempKey = a.toString();
					for (Text b : listB) {
						totalClicks += Integer.parseInt(b.toString());
					}
				}
				context.write(new Text(tempKey), new Text(totalClicks + ""));
			}
		}
	}

	public static class ReduceByProfile extends Reducer<Text, IntWritable, Text, DoubleWritable> {
        @Override
        protected void reduce(Text key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
            Double suma = 0.0;
            int cuenta = 1;
            Iterator<IntWritable> iter = values.iterator();
            while (iter.hasNext()) {
                IntWritable elem = iter.next();
                suma += elem.get();
                cuenta = cuenta + 1;

            }
            context.write(key, new DoubleWritable(suma / cuenta));
        }
    }


    public static void main(String[] args) throws IOException, ClassNotFoundException, InterruptedException, URISyntaxException {
        System.out.println("Uso: Uri(hdfs://hadoop-master:9000), InputPath(userid_profile.txt), InputPath(training_unir.txt), OutputPath(job1), OutputPath(job2)");
        args = new String[5];
        args[0] = "hdfs://172.18.0.2:9000";
        args[1] = "";
        args[2] = "";
        args[3] = "job1";
        args[4] = "job2";
        if (args.length == 5) {


            Configuration conf = new Configuration();
            URI uri = new URI(args[0]);
            FileSystem hdfs = FileSystem.get(uri, conf);
            //FileSystem hdfs = FileSystem.get(conf);
            Path workingDirectory = hdfs.getWorkingDirectory();
            args[1] = workingDirectory + "/userid_profile.txt";
            args[2] = workingDirectory + "/training_unir.txt";
            args[3] = workingDirectory + "/job1";
            args[4] = workingDirectory + "/job2";
            if (hdfs instanceof DistributedFileSystem) {
                System.out.println("HDFS is the underlain filesystem");
                Job job = Job.getInstance(conf, "task1_job1");
                Job job2 = Job.getInstance(conf, "task1_job2");

                // settings job1
                // El job 1 simplemente marca qué tipo de archivo es, si es A equivale a "userid_profile"
                // si es B equivale a "training_unir"
                // se realiza un "Join" para estos dos archivos, simplemente anteponiendo un marcador a cada linea.
                job.setJarByClass(MainTrabajoFinal.class);
                job.setOutputKeyClass(Text.class);
                job.setOutputValueClass(IntWritable.class);
                job.setMapperClass(SimpleMap.class);
                job.setReducerClass(CountAcceptanceCDT.class);

                // settings job2
                job2.setJarByClass(MainTrabajoFinal.class);
                job2.setOutputKeyClass(Text.class);
                job2.setOutputValueClass(IntWritable.class);
                job2.setMapperClass(SimpleMap.class);
                job2.setReducerClass(ReduceByProfile.class);

                //                                  inputUserProfile
                MultipleInputs.addInputPath(job, new Path(args[1]), TextInputFormat.class, CountMaritalMap.class);
                //                                  inputTraining
                MultipleInputs.addInputPath(job, new Path(args[2]), TextInputFormat.class, TrainingMap.class);
                FileOutputFormat.setOutputPath(job, new Path(args[3]));

                if (job.waitForCompletion(true)) {
                    System.out.println("Job1 terminado" +
                            "\n" + job.getHistoryUrl() +
                            "\n" + job.getTrackingURL() +
                            "\n" + job.getStatus().toString() +
                            "\n" + job.getFinishTime());

                    FileInputFormat.addInputPath(job2, new Path(args[3] + "/part-r-00000"));
                    FileOutputFormat.setOutputPath(job2, new Path(args[4]));
                    if (job2.waitForCompletion(true)) {
                        System.out.println("Job2 terminado" +
                                "\n" + job2.getHistoryUrl() +
                                "\n" + job2.getTrackingURL() +
                                "\n" + job2.getStatus().toString() +
                                "\n" + job2.getFinishTime());

                    } else {
                        System.err.println("Error en job2");
                    }

                } else {
                    System.err.println("Error en job1");
                }
            }
        } else {
            System.out.println("Error en el número de parámetros");
        }

    }
}
