package com.unir;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hdfs.DistributedFileSystem;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.MultipleInputs;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;


import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Iterator;

public class Main {


    public static class SimpleMap extends Mapper<LongWritable, Text, Text, IntWritable> {
        @Override
        protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
            String linea = value.toString();
            String[] data = linea.split("\t");

            try {
                context.write(new Text(data[0]), new IntWritable(Integer.parseInt(data[1])));
            }catch (Exception e){
                System.err.println("Ocurrio un error 1"+e);
            }

        }
    }

    public static class UserProfileMap extends Mapper<LongWritable, Text, Text, Text> {
        @Override
        protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
            String linea = value.toString();
            String[] data = linea.split("\t");

            try {
                context.write(new Text(data[0]), new Text("A"+"|"+data[1]+"-"+data[2])); // A as FileUserProfile
            } catch (Exception e) {
                System.err.println("Ocurrio un error A" + e);
            }

        }
    }

    public static class TrainingMap extends Mapper<LongWritable, Text, Text, Text> {
        @Override
        protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
            String linea = value.toString();
            String[] data = linea.split("\t");

            try {
                // Se calcula el CTR para cada usuario
                context.write(new Text(data[11]), new Text("B"+"|"+Integer.parseInt(data[0])/Integer.parseInt(data[1]))); //FileTraining AS B
            } catch (Exception e) {
                System.err.println("Ocurrio un error B" + e);
            }

        }
    }

    public static class ReduceByUserId extends Reducer<Text, Text, Text, Text> {

        private ArrayList<Text> listA = new ArrayList<Text>();
        private ArrayList<Text> listB = new ArrayList<Text>();

        @Override
        protected void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {

            String tempKey = "";
            int totalClicks = 0;
            //Cleaning the list
            listA.clear();
            listB.clear();
            Iterator<Text> iter = values.iterator();

            while (iter.hasNext()){
                Text next = iter.next();
                String data = next.toString();
                String[] dataOfValue = data.split("\\|");
                if (dataOfValue[0].equals("A")){
                    listA.add(new Text(dataOfValue[1]));
                } else if (dataOfValue[0].equals("B")){
                    listB.add(new Text(dataOfValue[1]));
                }
            }
            if (!listA.isEmpty() && !listB.isEmpty()) {
                totalClicks = 0;
                for(Text a : listA)  {
                    tempKey = a.toString();
                    for(Text b : listB)  {
                        totalClicks += Integer.parseInt(b.toString());
                    }
                }
                context.write(new Text(tempKey), new Text(totalClicks+""));
            }
        }
    }
    public static class ReduceByProfile extends Reducer<Text, IntWritable, Text, DoubleWritable> {
        @Override
        protected void reduce(Text key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
            Double suma = 0.0;
            int cuenta = 1;
            Iterator<IntWritable> iter = values.iterator();
            while (iter.hasNext()){
                IntWritable elem = iter.next();
                suma+=elem.get();
                cuenta=cuenta+1;

            }
            context.write(key, new DoubleWritable(suma/cuenta));
        }
    }


    public static void main(String[] args) throws IOException, ClassNotFoundException, InterruptedException, URISyntaxException {
        System.out.println("Uso: Uri(hdfs://hadoop-master:9000), InputPath(userid_profile.txt), InputPath(training_unir.txt), OutputPath(job1), OutputPath(job2)");
        args = new String[5];
        args[0]="hdfs://172.18.0.2:9000";
        args[1]="";
        args[2]="";
        args[3]="job1";
        args[4]="job2";
        if(args.length==5){


        Configuration conf = new Configuration();
        URI uri = new URI(args[0]);
        FileSystem hdfs = FileSystem.get(uri, conf);
        //FileSystem hdfs = FileSystem.get(conf);
        Path workingDirectory = hdfs.getWorkingDirectory();
            args[1]=workingDirectory+"/userid_profile.txt";
            args[2]=workingDirectory+"/training_unir.txt";
            args[3]=workingDirectory+"/job1";
            args[4]=workingDirectory+"/job2";
        if(hdfs instanceof DistributedFileSystem){
            System.out.println("HDFS is the underlain filesystem");
            Job job = Job.getInstance(conf, "task1_job1");
            Job job2 = Job.getInstance(conf, "task1_job2");

            // settings job1
            // El job 1 simplemente marca qué tipo de archivo es, si es A equivale a "userid_profile"
            // si es B equivale a "training_unir"
            // se realiza un "Join" para estos dos archivos, simplemente anteponiendo un marcador a cada linea.
            job.setJarByClass(Main.class);
            job.setOutputKeyClass(Text.class);
            job.setOutputValueClass(Text.class);
            job.setReducerClass(ReduceByUserId.class);

            // settings job2
            job2.setJarByClass(Main.class);
            job2.setOutputKeyClass(Text.class);
            job2.setOutputValueClass(IntWritable.class);
            job2.setMapperClass(SimpleMap.class);
            job2.setReducerClass(ReduceByProfile.class);

            //                                  inputUserProfile
            MultipleInputs.addInputPath(job, new Path(args[1]), TextInputFormat.class, UserProfileMap.class);
            //                                  inputTraining
            MultipleInputs.addInputPath(job, new Path(args[2]), TextInputFormat.class, TrainingMap.class);
            FileOutputFormat.setOutputPath(job, new Path(args[3]));

            if(job.waitForCompletion(true)){
                System.out.println("Job1 terminado" +
                        "\n"+job.getHistoryUrl()+
                        "\n"+job.getTrackingURL()+
                        "\n"+job.getStatus().toString()+
                        "\n"+job.getFinishTime());

                FileInputFormat.addInputPath(job2, new Path(args[3]+"/part-r-00000"));
                FileOutputFormat.setOutputPath(job2, new Path(args[4]));
                if(job2.waitForCompletion(true)){
                    System.out.println("Job2 terminado" +
                            "\n"+job2.getHistoryUrl()+
                            "\n"+job2.getTrackingURL()+
                            "\n"+job2.getStatus().toString()+
                            "\n"+job2.getFinishTime());

                }else{
                    System.err.println("Error en job2");
                }

            }else {
                System.err.println("Error en job1");
            }
        }

        /*
        job.setJarByClass(WordCount.class);
        job.setMapperClass(TokenizerMapper.class);
        job.setCombinerClass(IntSumReducer.class);
        job.setReducerClass(IntSumReducer.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);
        FileInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));
        System.exit(job.waitForCompletion(true) ? 0 : 1);

        */
        } else {
            System.out.println("Error en el número de parámetros");
        }

    }
}

/**
Click Impression    DisplayURL    AdID  AdvertiserID  Depth Position  QueryID   KeywordID TitleID  DescriptionID    UserID   * */
/**
 0-unknow   1-(0, 12]	0.197825704741253
 0-unknow   2-(12, 18]  0.2031884964051266
 0-unknow   3-(18, 24]	0.19020005798782255
 0-unknow   4-(24, 30]	0.21903428971308608
 0-unknow   5-(30, 40]	0.23785425101214575
 0-unknow   6-(40, &]	0.19667590027700832
 1-male     1-(0, 12]	0.1996473564089677
 1-male     2-(12, 18]  0.19944399634006765
 1-male     3-(18, 24]	0.19515772096634998
 1-male     4-(24, 30]	0.20035794780459648
 1-male     5-(30, 40]	0.22280084329354846
 1-male     6-(40, &]	0.24134225521270736
 2-female   1-(0, 12]	0.20782773826000192
 2-female   2-(12, 18]  0.189280616831829
 2-female   3-(18, 24]	0.22518820522843155
 2-female   4-(24, 30]	0.23611055117994803
 2-female   5-(30, 40]	0.2501255403694162
 2-female   6-(40, &]	0.2676403421626434

 * */