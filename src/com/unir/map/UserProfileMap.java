package com.unir.map;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

/**
 * Created by Juan Pablo Alvis Colmenares on 15/04/17. mailto:juanpabloalvis@gmail.com
 */
public class UserProfileMap extends Mapper<LongWritable, Text, Text, Text> {
    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        String linea = value.toString();
        String[] data = linea.split("\t");

        try {
            context.write(new Text(data[0]), new Text("A"+"|"+data[1]+data[2])); // A as FileUserProfile
        } catch (Exception e) {
            System.err.println("Ocurrio un error A" + e);
        }

    }
}