package com.unir.map;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

/**
 * Created by Juan Pablo Alvis Colmenares on 15/04/17. mailto:juanpabloalvis@gmail.com
 */
public class TrainingMap extends Mapper<LongWritable, Text, Text, Text> {
    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        String linea = value.toString();
        String[] data = linea.split("\t");

        try {
            context.write(new Text(data[0]), new Text("B"+"|"+data[6])); //FileTraining AS B
        } catch (Exception e) {
            System.err.println("Ocurrio un error B" + e);
        }

    }
}
