package com.unir.map;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

/**
 * Created by Juan Pablo Alvis Colmenares on 15/04/17. mailto:juanpabloalvis@gmail.com
 */
public class SimpleMap extends Mapper<LongWritable, Text, Text, IntWritable> {
    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        String linea = value.toString();
        String[] data = linea.split("\t");

        try {
            context.write(new Text(data[0]), new IntWritable(Integer.parseInt(data[1])));
        }catch (Exception e){
            System.err.println("Ocurrio un error 1"+e);
        }

    }
}
